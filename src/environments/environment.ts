// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
   production: false,
  firebase: {
    apiKey: 'AIzaSyAosEqFKsWQrR-to8wpsPInIs-mzeCaATc',
    authDomain: 'proplisting-1abee.firebaseapp.com',
    databaseURL: 'https://proplisting-1abee.firebaseio.com',
    projectId: 'proplisting-1abee',
    storageBucket: 'proplisting-1abee.appspot.com',
    messagingSenderId: '759426015897'
  }

};
